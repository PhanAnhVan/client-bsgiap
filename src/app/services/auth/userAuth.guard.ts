import { Injectable } from '@angular/core'
import { CanActivate, Router } from '@angular/router'
import { Observable } from 'rxjs'
import { Globals } from '../../globals'

@Injectable()
export class UserAuthGuard implements CanActivate {
    constructor(private router: Router, private globals: Globals) {}
    canActivate(): Observable<boolean> | Promise<boolean> | boolean {
        if (this.globals.CUSTOMER.check()) {
            let skip = true

            this.globals.CUSTOMER._check().subscribe((res: any) => {
                skip = res.skip

                if (!skip) {
                    this.globals.CUSTOMER.remove(true)
                    this.router.navigate(['/'])
                } else {
                    this.globals.CUSTOMER.set(res.data)
                }
            })
            return skip
        } else {
            this.router.navigate(['/'])
        }
    }
}
