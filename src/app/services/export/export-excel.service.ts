import { Injectable } from '@angular/core';
// import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';

import { TranslateService } from '@ngx-translate/core';
// const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';

// const EXCEL_EXTENSION = '.xlsx';

@Injectable({
    providedIn: 'root'
})

export class ExcelService {

    constructor(private _translate: TranslateService) { }

    excelConfig = {
        name: '',
        id: '',
        cols: [],
        data: [],

        configCols: (list) => {
            if (list.length > 0) {
                list = list.filter((res: any) => { if (res.title != '' && res.show == true) return true });
                this.excelConfig.cols = list.reduce((n, o, i) => {
                    if (o.show && o.field != 'action') {
                        o.title = this._translate.instant(o.title);
                        n[i] = o;
                    }
                    return n;
                }, []);
            }
        },

        configData: (list, customize) => {
            for (let i = 0; i < list.length; i++) {
                let item = list[i];
                let obj = {};
                for (let j = 0; j < this.excelConfig.cols.length; j++) {
                    let col = this.excelConfig.cols[j];
                    if (item[col.field] || item[col.field] == 0 || col.field == 'index' || !item[col.field]) {
                        let value = item[col.field] ? (typeof item[col.field] == 'string' ? (item[col.field] || '') : item[col.field]) : '';
                        if (typeof item[col.field] == 'number') {
                            value = item[col.field].toLocaleString('en');
                        }
                        if (col['change'] && col['change'] == true) {
                            value = this._translate.instant(customize[col.field][value]);
                        }
                        if (item[col.field] && col['date'] && col['date'] == true) {
                            value = this.excelConfig.formatDate(value, col['short'], false);
                        }
                        if (col.field == 'index') {
                            value = i + 1;
                        }

                        obj[col.title] = value;
                    }
                }
                this.excelConfig.data.push(obj);
            }
        },

        formatDate: (value, short, time = true) => {
            let date = new Date(value);
            if (time == true) {
                return (!short ? (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':' + (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) : '') + ' ' + (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()) + '/' + ((date.getMonth() < 10 ? '0' : '') + (date.getMonth() + 1)) + '/' + date.getFullYear()
            } else {
                return (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()) + '/' + ((date.getMonth() < 10 ? '0' : '') + (date.getMonth() + 1)) + '/' + date.getFullYear()
            }
        },

        configName: (value) => {
            value = value.replace(/[\]\[\?\*\/\\]/g, "");
            if (value.length < 31) {
                return value;
            }
            return value.substring(0, 28) + '...';
        },

        reset: () => {
            this.excelConfig.cols = [];
            this.excelConfig.data = [];
        }
    }

    public ini(listCols: any = [], data: any, excelFileName: string, customize: any = []) {
        this.excelConfig.reset();
        this.excelConfig.name = this.excelConfig.configName(excelFileName);
        if (typeof data != 'string') {
            this.excelConfig.configCols(listCols);
            this.excelConfig.configData(data, customize);
        } else {
            this.excelConfig.id = data;
        }
        this.export();
    }

    public export(): void {
        if (this.excelConfig.id == '') {
            let json = this.excelConfig.data;
            const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
            const workbook: XLSX.WorkBook = XLSX.utils.book_new();
            XLSX.utils.book_append_sheet(workbook, worksheet, this.excelConfig.name);
            XLSX.writeFile(workbook, this.excelConfig.name + '.xlsx');
        } else {
            /* create new workbook */
            var workbook = XLSX.utils.book_new();
            /* convert table 'table1' to worksheet named "Sheet1" */
            var ws1 = XLSX.utils.table_to_sheet(document.getElementById(this.excelConfig.id));
            XLSX.utils.book_append_sheet(workbook, ws1, this.excelConfig.name);
            /* convert table 'table2' to worksheet named "Sheet2" */
            // var ws2 = XLSX.utils.table_to_sheet(document.getElementById('table2'));
            // XLSX.utils.book_append_sheet(workbook, ws2, "Sheet2");
            XLSX.writeFile(workbook, this.excelConfig.name + '.xlsx');
        }
        // const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };

        // const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });

        // this.saveAsExcelFile(excelBuffer, this.excelConfig.name);
    }

    public _read(e) {
        /* read workbook */
        const bstr: string = e.target.result;
        try {
            const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
            /* grab first sheet */
            const wsname: string = wb.SheetNames[0];
            const ws: XLSX.WorkSheet = wb.Sheets[wsname];
            return XLSX.utils.sheet_to_json(ws, { header: 1 });
        } catch (e) {
            return false;
        }
    }

    // private saveAsExcelFile(buffer: any, fileName: string): void {

    //     const data: Blob = new Blob([buffer], {

    //         type: EXCEL_TYPE
    //     });

    //     FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);

    // }
}
