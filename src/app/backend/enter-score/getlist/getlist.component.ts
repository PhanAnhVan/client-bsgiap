import { Component, Input, OnChanges, OnDestroy, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { BsModalService } from 'ngx-bootstrap/modal'
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service'
import { ToastrService } from 'ngx-toastr'
import { Globals } from '../../../globals'
import { TableService } from '../../../services/integrated/table.service'
import { AlertComponent } from '../../modules/alert/alert.component'

@Component({
    selector: 'app-getlist',
    templateUrl: './getlist.component.html'
})
export class GetlistComponent implements OnInit, OnChanges, OnDestroy {
    public connect

    modalRef: BsModalRef

    @Input('filter') filter: any

    @Input('change') change: any

    public class_id: number = 0

    public product_id: number = 0

    public page_id: number = 0

    public product_name: string = ''

    public class_name: string = ''

    public lock: any

    public skip: boolean = false

    public token: any = {
        getCourse: 'get/score/getCourseClass',
        lock: 'set/score/lockCourse',
        endCourse: 'set/score/endCourse',
        calculateAVG: 'set/score/calculateAVG'
    }

    private cols = [
        { title: 'lblStt', field: 'index', show: true },
        { title: 'score.name', field: 'name', show: true, filter: true },
        { title: 'score.totalStudent', field: 'total_student', show: true, filter: true },
        { title: 'score.totalPass', field: 'total_pass', show: true, filter: true },
        { title: 'score.totalNoPass', field: 'total_nopass', show: true, filter: true },
        { title: 'score.totalNoScore', field: 'total_noscore', show: true, filter: true },
        { title: '#', field: 'action', show: true }
    ]

    public cstable = new TableService()

    constructor(public globals: Globals, private modalService: BsModalService, public toastr: ToastrService, public router: Router) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'getCourse':
                    if (res.status == 1) {
                        this.cstable.data = []
                        this.course._checkConfirm(res.data)
                        this.cstable._concat(res.data, true)
                    }
                    break
                case 'lock':
                case 'endCourse':
                    let type = res.status == 1 ? 'success' : res.status == 0 ? 'warning' : 'danger'
                    this.toastr[type](res.message, type, { timeOut: 1500 })
                    if (res.status == 1) {
                        this.globals.send({
                            path: this.token.getCourse,
                            token: 'getCourse',
                            params: {
                                class_id: this.class_id,
                                teacher_id: this.globals.USERS.get(true).type == 1 ? this.globals.USERS.get(true).id : 0
                            }
                        })
                    }
                    break

                case 'calculateAVG':
                    type = res.status == 1 ? 'success' : res.status == 0 ? 'warning' : 'danger'
                    this.toastr[type](res.message, type, { timeOut: 1500 })
                    if (res.status == 1) {
                        this.course.flags = false
                    }
                    break
                default:
                    break
            }
        })
    }

    ngOnInit() {
        this.cstable._ini({ cols: this.cols, data: [], keyword: 'content', count: 50 })
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    onChangeProcess(e) {
        this.skip = e

        return
        this.globals.send({
            path: this.token.getCourse,
            token: 'getCourse',
            params: { class_id: this.class_id, teacher_id: this.globals.USERS.get(true).type == 1 ? +this.globals.USERS.get(true).id : 0 }
        })
    }

    ngOnChanges() {
        if (typeof this.filter === 'object') {
            if (this.filter.class_id > 0) {
                this.class_id = +this.filter.class_id
                this.page_id = +this.filter.page_id
                this.class_name = this.filter.class_name
                this.skip = this.filter.skip
                this.cstable.data = []
                this.globals.send({
                    path: this.token.getCourse,
                    token: 'getCourse',
                    params: {
                        class_id: this.filter.class_id,
                        teacher_id: this.globals.USERS.get(true).type == 1 ? this.globals.USERS.get(true).id : 0
                    }
                })
            }
        }
    }

    onConfirm(product_id, lock) {
        this.modalRef = this.modalService.show(AlertComponent, {
            initialState: { messages: lock != 1 ? 'score.confirm' : 'score.unconfirm' }
        })
        this.modalRef.content.onClose.subscribe(result => {
            if (result == true) {
                let data = lock != 1 ? 1 : 0

                this.globals.send({
                    path: this.token.lock,
                    token: 'lock',
                    data: data,
                    params: { product_id: product_id, class_id: this.class_id }
                })
            }
        })
    }

    onEndCourse(product_id) {
        this.modalRef = this.modalService.show(AlertComponent, { initialState: { messages: 'score.endCourse' } })
        this.modalRef.content.onClose.subscribe(result => {
            if (result == true) {
                this.globals.send({
                    path: this.token.endCourse,
                    token: 'endCourse',
                    params: { product_id: product_id, class_id: this.class_id }
                })
            }
        })
    }

    course = {
        skip: false,
        flags: false,
        _checkConfirm: data => {
            let d = 0
            data.filter((res: any) => (res.status_end == 2 ? d++ : ''))
            this.course.skip = d == data.length ? true : false
        },

        _setListProduct: data => {
            let list = []
            for (let i = 0; i < data.length; i++) {
                list.push(data[i].product_id)
            }
            return JSON.stringify(list)
        },

        _onAvg: () => {
            if (!this.course.flags) {
                this.course.flags = true
                let listProduct = this.course._setListProduct(this.cstable.data)
                this.globals.send({
                    path: this.token.calculateAVG,
                    token: 'calculateAVG',
                    data: listProduct,
                    params: { page_id: this.page_id, class_id: this.class_id }
                })
            }
        }
    }

    handleEndCourse = (status: number, idProduct: number, noScore: number) => {
        if (status == 2) return

        if (noScore > 0) {
            this.modalRef = this.modalService.show(AlertComponent, {
                initialState: { messages: 'score.noScoreNotification', name: noScore }
            })

            return
        }

        this.onEndCourse(idProduct)
    }
}
