import { Component, OnDestroy, OnInit } from '@angular/core'
import { BsModalService } from 'ngx-bootstrap/modal'
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service'
import { ToastrService } from 'ngx-toastr'
import { Globals } from '../../../globals'
import { TableService } from '../../../services/integrated/table.service'
import { AlertComponent } from '../../modules/alert/alert.component'
import { ExcelService } from './../../../services/export/export-excel.service'

@Component({
    selector: 'app-getlist',
    templateUrl: './getlist.component.html',
    styleUrls: ['./getlist.component.css']
})
export class GetlistComponent implements OnInit, OnDestroy {
    public connect

    modalRef: BsModalRef

    loggedType: number
    loggedId: number

    private token = {
        getlist: 'get/user/getlist',
        remove: 'set/user/remove'
    }

    private cols = [
        { title: 'lblStt', field: 'index', show: true },
        { title: 'user.avatar', field: 'avatar', show: true },
        { title: 'user.name', field: 'name', show: true, filter: true },
        { title: 'lblPhone', field: 'phone', show: true, filter: true },
        { title: 'lblEmail', field: 'email', show: true, filter: true },
        { title: 'lblMaker_date', field: 'maker_date', show: true, filter: true },
        { title: 'lblTypePersonal', field: 'type', show: true, filter: true },
        { title: 'lblAction', field: 'action', show: true }
    ]

    public cwstable = new TableService()

    constructor(
        public globals: Globals,
        public toastr: ToastrService,
        public modalService: BsModalService,
        public ExcelService: ExcelService
    ) {
        this.loggedType = +this.globals.USERS.get(true).type
        this.loggedId = +this.globals.USERS.get(true).id

        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res['token']) {
                case 'getlist':
                    if (this.loggedType === 1) {
                        const filtered = res.data.filter((item: { id: string }) => +item.id === this.loggedId)
                        this.cwstable._concat(filtered, true)
                    } else {
                        this.cwstable._concat(res.data, true)
                    }
                    break

                case 'remove':
                    this.showNotification(res)
                    if (res.status == 1) {
                        this.globals.send({ path: this.token.getlist, token: 'getlist' })
                    }
                    break

                default:
                    break
            }
        })
    }

    ngOnInit() {
        this.cwstable._ini({ cols: this.cols, data: [], keyword: 'user' })

        this.globals.send({
            path: this.token.getlist,
            token: 'getlist'
        })
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    showNotification(res) {
        let type = res.status == 1 ? 'success' : res.status == 0 ? 'warning' : 'danger'
        this.toastr[type](res.message, type, { timeOut: 1500 })
    }

    // hidden all type = 0, 2 with condition type = 2 and id logged != id account
    processUpdate(type: number, id: number) {
        if (this.loggedType === 2 && (type === 0 || type === 2) && this.loggedId !== id) {
            return false
        } else return true
    }

    processRemove(type: number, id: number) {
        if ((this.loggedType === 2 && (type === 0 || type === 2) && this.loggedId !== id) || this.loggedId === id) {
            return false
        } else return true
    }

    onRemove(id: number, name: any) {
        this.modalRef = this.modalService.show(AlertComponent, { initialState: { messages: 'user.remove', name: name } })
        this.modalRef.content.onClose.subscribe(result => {
            if (result == true) {
                this.globals.send({ path: this.token.remove, token: 'remove', params: { id: id } })
            }
        })
    }

    handleExportClick = () => {
        const TEACHER_TYPE = 1,
            FILE_NAME = 'Danh sách Giảng viên'

        const cols = [
            { title: 'lblStt', field: 'index', show: true },
            { title: 'CBGVCode', field: 'code', show: true },
            { title: 'user.name', field: 'name', show: true },
            { title: 'teacher.formatWork', field: 'format_work', show: true },
            { title: 'teacher.academicLevel', field: 'academic_level', show: true },
            { title: 'user.training_fields', field: 'training_fields', show: true },
            { title: 'user.teacher', field: 'teacher', show: true },
            { title: 'user.parent', field: 'parent_name', show: true },
            { title: 'user.status', field: 'status', show: true },
            { title: 'user.action', field: 'actions', show: true }
        ]
        this.cwstable.cachedList.filter(item => {
            item.status = item.status == 1 ? 'Đang công tác' : 'Ngừng công tác';
            item.action = '';
        })

        this.ExcelService.ini(cols, this.cwstable.cachedList, FILE_NAME)
    }
}
