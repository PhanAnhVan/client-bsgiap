import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { RouterModule, Routes } from '@angular/router'
import { TranslateModule } from '@ngx-translate/core'
import { GetListLanguageComponent } from './getlist-language/getlist-language.component'
import { ProcessLanguageComponent } from './process-language/process-language.component'

export const routes: Routes = [
    { path: '', redirectTo: 'get-list' },
    { path: 'get-list', component: GetListLanguageComponent },
    { path: 'insert', component: ProcessLanguageComponent },
    { path: 'update', component: ProcessLanguageComponent }
]
@NgModule({
    imports: [CommonModule, RouterModule.forChild(routes), FormsModule, ReactiveFormsModule, TranslateModule],
    declarations: [GetListLanguageComponent, ProcessLanguageComponent]
})
export class LanguagesModule {}
