import { Component, OnDestroy, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router'
import { TranslateService } from '@ngx-translate/core'
import { ToastrService } from 'ngx-toastr'
import { Globals } from '../../../../globals'
import { LinkService } from '../../../../services/integrated/link.service'
import { TagsService } from '../../../../services/integrated/tags.service'
import { uploadFileService } from '../../../../services/integrated/upload.service'

@Component({
    selector: 'app-process-language',
    templateUrl: './process-language.component.html'
})
export class ProcessLanguageComponent implements OnInit, OnDestroy {
    fm: FormGroup
    public connect: any
    public token: any = {
        process: 'set/languages/process',
        pathGetRowLanguages: 'get/languages/getrow'
    }
    public id: number
    public listLanguages: any = []
    public icon = new uploadFileService()
    constructor(
        public fb: FormBuilder,
        public router: Router,
        public toastr: ToastrService,
        public globals: Globals,
        private routerAct: ActivatedRoute,
        public translate: TranslateService,
        public tags: TagsService,
        public link: LinkService
    ) {
        if (this.globals.USERS.get(true).type == 0) {
            this.routerAct.queryParams.subscribe(params => {
                if (params && params.id) {
                    this.id = this.globals.crypto.decrypt(params.id)
                }
                if (this.id && this.id != 0) {
                    this.getRow()
                } else {
                    this.fmConfigs()
                }
            })
        } else {
            this.router.navigate([this.globals.admin])
        }

        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res['token']) {
                case 'GetRowLanguages':
                    let data = res['data']
                    this.fmConfigs(data)
                    break

                case 'languagesProcess':
                    this.showNotification(+res.status, res.message)
                    if (res.status === 1) {
                        setTimeout(() => {
                            this.router.navigate([this.globals.admin + '/setting/languages/get-list'])
                        }, 1000)
                    }
                    break

                default:
                    break
            }
        })
    }

    ngOnInit() {
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    fmConfigs(item: any = '') {
        item = typeof item === 'object' ? item : { status: 1, parent_id: 0, orders: 0 }

        this.fm = this.fb.group({
            name: [item.name ? item.name : '', [Validators.required]],
            code: [item.code ? item.code : '', [Validators.required]],
            status: item.status && item.status == 1 ? true : false
        })

        const iconConfig = { path: this.globals.BASE_API_URL + 'public/language/', data: item.icon ? item.icon : '' }

        this.icon._ini(iconConfig)

        this.tags._set(item.keywords ? JSON.parse(item['keywords']) : '')
    }

    showNotification(status: number, message: string) {
        let type = status == 1 ? 'success' : status == 0 ? 'warning' : 'danger'
        this.toastr[type](message, type, { timeOut: 1500 })
    }

    getRow() {
        this.globals.send({ path: this.token.pathGetRowLanguages, token: 'GetRowLanguages', params: { id: this.id } })
    }

    onSubmit() {
        if (this.fm.valid) {
            let data = this.fm.value

            data.status = data.status == true ? 1 : 0

            data.icon = this.icon._get(true)

            this.globals.send({
                path: this.token.process,
                token: 'languagesProcess',
                data: data,
                params: { id: this.id || 0 }
            })
        }
    }
}
