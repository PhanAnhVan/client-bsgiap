import { Component, OnDestroy, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal'
import { ToastrService } from 'ngx-toastr'
import { Globals } from '../../../../globals'
import { TableService } from '../../../../services/integrated/table.service'
import { AlertComponent } from '../../../modules/alert/alert.component'

@Component({
    selector: 'app-getlist-language',
    templateUrl: './getlist-language.component.html'
})
export class GetListLanguageComponent implements OnInit, OnDestroy {
    public connect: any

    public type: number = 0

    public translateTitle: string = ''

    modalRef: BsModalRef

    public id: number

    public token: any = {
        getlist: 'get/languages/getlist',
        remove: 'set/languages/remove',
        changestatus: 'set/languages/changeStatus'
    }

    private cols = [
        { title: 'lblStt', field: 'index', show: true },
        { title: 'lblName', field: 'name', show: true, filter: true },
        { title: 'languages.code', field: 'code', show: true, filter: true },
        { title: 'lblMaker_date', field: 'maker_date', show: true, filter: true },
        { title: 'lblAction', field: 'action', show: true }
    ]

    public cwstable = new TableService()

    constructor(
        private modalService: BsModalService,
        public routerAtc: ActivatedRoute,
        public router: Router,
        public toastr: ToastrService,
        public globals: Globals
    ) {
        this.cwstable._ini({ cols: this.cols, data: [], keyword: 'getListLanguages', count: 50 })

        if (this.globals.USERS.get(true).type == 0) {
            this.getList()
        } else {
            this.router.navigate([this.globals.admin])
        }

        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'getListLanguages':
                    this.cwstable.data = []
                    this.cwstable.sorting = { field: 'maker_date', sort: 'DESC', type: '' }
                    this.cwstable._concat(res.data, true)
                    break

                case 'removeLanguages':
                    this.showNotification(res)
                    if (res.status == 1) {
                        this.cwstable._delRowData(this.id)
                    }
                    break

                case 'changestatus':
                    this.showNotification(res)
                    if (res.status == 1) {
                        this.getList()
                    }
                    break

                default:
                    break
            }
        })
    }

    ngOnInit() {}

    getList = () =>
        this.globals.send({
            path: this.token.getlist,
            token: 'getListLanguages'
        })

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    showNotification(res) {
        let type = res.status == 1 ? 'success' : res.status == 0 ? 'warning' : 'danger'
        this.toastr[type](res.message, type, { timeOut: 1500 })
    }

    onRemove(item: any): void {
        this.id = item.id
        this.modalRef = this.modalService.show(AlertComponent, { initialState: { messages: 'languages.remove', name: item.name } })
        this.modalRef.content.onClose.subscribe(result => {
            if (result == true) {
                this.globals.send({ path: this.token.remove, token: 'removeLanguages', params: { id: item.id } })
            }
        })
    }

    changeStatus = (id, status) => {
        this.globals.send({ path: this.token.changestatus, token: 'changestatus', params: { id: id, status: status == 1 ? 0 : 1 } })
    }
}
