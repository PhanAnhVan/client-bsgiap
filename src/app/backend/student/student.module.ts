import { CommonModule, DatePipe } from '@angular/common'
import { NgModule } from '@angular/core'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { RouterModule, Routes } from '@angular/router'
import { TranslateModule } from '@ngx-translate/core'
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker'
import { ServicePipeModule } from '../../services/pipe'
import { ChangesPasswordComponent } from './changespassword/changespassword.component'
import { GetlistComponent } from './getlist/getlist.component'
import { ImportStudentsComponent } from './import/import.component'
import { ProcessCustomerComponent } from './process/process.component'

const routes: Routes = [
    { path: '', redirectTo: 'get-list' },
    { path: 'get-list', component: GetlistComponent },
    { path: 'insert', component: ProcessCustomerComponent },
    { path: 'import', component: ImportStudentsComponent },
    { path: 'update/:id', component: ProcessCustomerComponent },
    { path: 'changespassword/:id', component: ChangesPasswordComponent }
]

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes),
        TranslateModule,
        BsDatepickerModule.forRoot(),
        ServicePipeModule
    ],
    declarations: [GetlistComponent, ProcessCustomerComponent, ChangesPasswordComponent, ImportStudentsComponent],
    providers: [DatePipe]
})
export class StudentModule {}
