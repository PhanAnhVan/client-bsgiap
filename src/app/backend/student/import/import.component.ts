import { Component, OnDestroy, OnInit } from '@angular/core'
import { TranslateService } from '@ngx-translate/core'
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal'
import { ToastrService } from 'ngx-toastr'
import { Globals } from 'src/app/globals'
import * as XLSX from 'xlsx'
import { TableService } from './../../../services/integrated/table.service'
import { ToslugService } from './../../../services/integrated/toslug.service'
import { AlertComponent } from './../../modules/alert/alert.component'

@Component({
    selector: 'app-import',
    templateUrl: './import.component.html',
    styleUrls: ['./import.component.scss'],
    providers: [ToslugService]
})
export class ImportStudentsComponent implements OnInit, OnDestroy {
    private connect
    private token = {
        getStudentsFilter: 'get/customer/getStudentsFilter',
        importStudents: 'get/customer/importStudents',
        getStudentLargestCode: 'get/customer/getStudentLargestCode'
    }
    private modalRef: BsModalRef

    lengthExcelFile: number = 0
    step: number = 1
    processTitle: string = ''
    largestCode: string = ''
    data: [][]
    listFinal = {
        data: [],
        search: '',
        field: ['name']
    }
    dataResult: any[]
    isShow: boolean = false
    isLoading: boolean = false

    cstable = new TableService()
    private cols = [
        { title: '#', field: 'action', show: true },
        { title: 'student.name', field: 'name', show: true, filter: true },
        { title: 'user.sex', field: 'sex', show: true, filter: true },
        { title: 'CMND', field: 'type_idno_1', show: true, filter: true },
        { title: 'CCCD', field: 'type_idno_2', show: true, filter: true },
        { title: 'Hộ chiếu', field: 'type_idno_3', show: true, filter: true },
        { title: 'student.idno_date', field: 'idno_date', show: true },
        { title: 'student.idno_place', field: 'idno_place', show: true, filter: true },
        { title: 'user.birth_date', field: 'birth_date', show: true },
        { title: 'user.address', field: 'address', show: true, filter: true },
        { title: 'lblPhone', field: 'phone', show: true },
        { title: 'lblEmail', field: 'email', show: true },
        { title: 'user.password', field: 'password', show: true },
        { title: 'student.facebook', field: 'facebook', show: true },
        { title: 'student.nation', field: 'nation', show: true, filter: true },
        { title: 'student.is_poors', field: 'is_poor_1', show: true, filter: true },
        { title: 'student.is_poor', field: 'is_poor_0', show: true, filter: true },
        { title: 'student.is_revolution', field: 'is_revolution', show: true, filter: true },
        { title: 'student.object_name', field: 'object_name', show: true },
        { title: 'student.code_company', field: 'code_company', show: true, filter: true },
        { title: 'student.academic_level', field: 'academic_level', show: true, filter: true },
        { title: 'student.height', field: 'height', show: true },
        { title: 'student.weight', field: 'weight', show: true }
    ]

    arrIncludes = [
        'name',
        'sex',
        'type_idno_1',
        'type_idno_2',
        'type_idno_3',
        'idno_date',
        'idno_place',
        'birth_date',
        'address',
        'nation',
        'is_poor_1',
        'is_poor_0',
        'is_revolution',
        'academic_level'
    ]

    constructor(
        private globals: Globals,
        private modalService: BsModalService,
        private toastr: ToastrService,
        public translate: TranslateService,
        private toSlug: ToslugService
    ) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'getStudentsFilter':
                    if (res.status !== 1) return

                    if (res.data?.length > 0) {
                        const response = res.data.filter((v, i, a) => a.findIndex(t => t.idno === v.idno && t.name === v.name) === i)
                        //TODO: filter duplicate values

                        const lengthStudents = this.listFinal.data.length - 1
                        const lengthResponse = response.length

                        for (let i = lengthStudents; i >= 0; i--) {
                            for (let j = 0; j < lengthResponse; j++) {
                                if (this.listFinal.data[i] && this.listFinal.data[i].name === response[j].name) {
                                    this.listFinal.data[i].isValid = true
                                    this.listFinal.data[i].isChecked = false
                                }
                            }
                        }
                    }

                    this.listFinal.data.forEach(student => {
                        // check if student has key isValid and isChecked, if yes then return
                        if (student.isValid || student.isChecked) return

                        student.name === null ||
                        (student.type_idno_1?.length > 0 && student.type_idno_2?.length > 0) ||
                        (student.type_idno_1?.length > 0 && student.type_idno_3?.length > 0) ||
                        (student.type_idno_2?.length > 0 && student.type_idno_3?.length > 0) ||
                        (student.type_idno_1?.length > 0 && student.type_idno_2?.length > 0 && student.type_idno_3?.length > 0) ||
                        (student.type_idno_1 === null && student.type_idno_2 === null && student.type_idno_3 === null) ||
                        student.sex === null ||
                        (student.sex?.length && this.toSlug._ini(student.sex) !== 'nam' && this.toSlug._ini(student.sex) !== 'nu') ||
                        student.idno_date === null ||
                        student.idno_place === null ||
                        student.birth_date === null ||
                        student.address === null ||
                        student.nation === null ||
                        (student.is_poor_0 === null && student.is_poor_1 === null) ||
                        (student.is_poor_0?.length &&
                            this.toSlug._ini(student.is_poor_0) !== 'co' &&
                            this.toSlug._ini(student.is_poor_0) !== 'khong') ||
                        (student.is_poor_1?.length &&
                            this.toSlug._ini(student.is_poor_1) !== 'co' &&
                            this.toSlug._ini(student.is_poor_1) !== 'khong') ||
                        student.is_revolution === null ||
                        (student.is_revolution?.length &&
                            this.toSlug._ini(student.is_revolution) !== 'co' &&
                            this.toSlug._ini(student.is_revolution) !== 'khong') ||
                        student.academic_level === null
                            ? ((student.isValid = true), (student.isChecked = false))
                            : ((student.isValid = false), (student.isChecked = true))
                    })

                    this.listFinal.data.sort((a, b) => {
                        if (a.isChecked && !b.isChecked) return -1
                        if (!a.isChecked && b.isChecked) return 1
                        return 0
                    })

                    this.cstable._ini({
                        cols: this.cols,
                        data: this.listFinal.data,
                        count: this.listFinal.data.length
                    })

                    this.processTitle = 'Danh sách dữ liệu'

                    this.isLoading = false
                    this.step = 2

                    this.globals.send({
                        path: this.token.getStudentLargestCode,
                        token: 'getStudentLargestCode'
                    })
                    break

                case 'importStudents':
                    this.showNotification(res)

                    if (res.status !== 1) return

                    this.isLoading = false
                    this.step = 3
                    break

                case 'getStudentLargestCode':
                    if (res.status !== 1 || res.data.length === 0) return

                    this.largestCode = res.data
                    break

                default:
                    break
            }
        })
    }

    ngOnInit() {
        this.cstable._ini({
            cols: this.cols,
            data: []
        })
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    showNotification(res) {
        let type = res.status == 1 ? 'success' : res.status == 0 ? 'warning' : 'danger'
        this.toastr[type](res.message, type, { timeOut: 1500 })
    }

    onFileChange = (e: { target: DataTransfer }) => {
        this.isLoading = true //TODO: show loading when start import

        const target: DataTransfer = <DataTransfer>e.target

        if (target.files.length !== 1) throw new Error('Cannot use multiple files')

        const reader: FileReader = new FileReader()

        reader.onload = (e: any) => {
            const bstr: string = e.target.result

            const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' })

            const wsname: string = wb.SheetNames[0]

            const ws: XLSX.WorkSheet = wb.Sheets[wsname]

            const resultFileReading: [][] = XLSX.utils.sheet_to_json(ws, { header: 1 })

            const resultExcelProcessing: [] = this._excelProcessing(resultFileReading)

            if (!resultExcelProcessing.length) return

            const resultAnalysis: [][] = this._analysisResultExcel(resultExcelProcessing)

            this.lengthExcelFile = resultAnalysis.length

            // const resultValidate: [][] = this._handleValidate(resultAnalysis)

            const resultUnique: [][] = this._uniqueResultAnalysis(resultAnalysis)

            this.listFinal.data = resultUnique

            return this._handleNextStep(resultUnique)
        }

        reader.readAsBinaryString(target.files[0])
    }

    private _excelProcessing = (list): [] => {
        const result: [] = list.slice(1) //TODO: result header column removed
        const LINE_LIMIT = 500

        if (!result?.length || result.length > LINE_LIMIT) {
            this._notificationError(result.length > LINE_LIMIT ? 'student.morethan500' : 'student.excelNotValue')

            this.isLoading = false

            return []
        }

        const resultConverted = JSON.stringify(result) //TODO: handle field value empty -> show field is null
        return JSON.parse(resultConverted)
    }

    private _analysisResultExcel = list => {
        const length = list.length
        let array = []

        for (let i = 0; i < length; i++) {
            let obj = {
                name: null,
                sex: null,
                type_idno_1: null,
                type_idno_2: null,
                type_idno_3: null,
                idno_date: null,
                idno_place: null,
                birth_date: null,
                address: null,
                phone: null,
                email: null,
                password: null,
                facebook: null,
                nation: null,
                is_poor_1: null,
                is_poor_0: null,
                is_revolution: null,
                object_name: null,
                code_company: null,
                academic_level: null,
                height: null,
                weight: null
            }

            if (list[i].length) {
                list[i].forEach((value: string | null, index: number) => {
                    obj[Object.keys(obj)[index]] = value?.length ? value.trim() : null
                })

                array.push(obj)
            }
        }

        return array
    }

    private _uniqueResultAnalysis = list => {
        const length = list.length
        let array = []

        for (let i = 0; i < length; i++) {
            array.push(
                Object.assign(list[i], {
                    idno: list[i].type_idno_1?.length
                        ? list[i].type_idno_1
                        : list[i].type_idno_2?.length
                        ? list[i].type_idno_2
                        : list[i].type_idno_3,
                    password: list[i].password?.length ? list[i].password : '123456789',
                })
            )
        }

        const uniqueArray = array.filter((v, i, a) => a.findIndex(t => t.idno === v.idno && t.name === v.name) === i)
        return uniqueArray
    }

    private _notificationError = (message: string) =>
        (this.modalRef = this.modalService.show(AlertComponent, {
            initialState: { messages: message }
        }))

    private _handleNextStep = list => {
        let array = []
        const length = list.length

        for (let i = 0; i < length; i++) {
            array.push({ name: list[i].name, idno: list[i].idno })
        }

        this.globals.send({
            path: this.token.getStudentsFilter,
            token: 'getStudentsFilter',
            data: array
        })

        return
    }

    handleCheckAll = () => (this.cstable.data.every(item => item.isChecked === true) ? true : false)

    handleCheckAllChange = (e: boolean) =>
        this.cstable.data.forEach(item => {
            if (item.isValid) return

            item.isChecked = e
        })

    handleSubmit = () => {
        let array = []

        this.cstable.data.forEach(item => {
            if (item.isChecked) {
                array.push(item)
            }
        })

        if (!array.length) {
            return (this.modalRef = this.modalService.show(AlertComponent, {
                initialState: { messages: 'Bạn chưa chọn dữ liệu nào' }
            }))
        }

        this.modalRef = this.modalService.show(AlertComponent, {
            initialState: { messages: 'student.import', name: array.length }
        })

        this.modalRef.content.onClose.subscribe(() => {
            this.isLoading = true
            let code = +this.largestCode.slice(4)

            this.cstable._ini({
                cols: this.cols,
                data: array,
                count: array.length
            })

            array.forEach(item => {
                item.type_idno = item.type_idno_1?.length ? '1' : item.type_idno_2?.length ? '2' : '3'
                item.idno = item.idno

                item.is_poor = this.toSlug._ini(item.is_poor_1) === 'co' ? '1' : this.toSlug._ini(item.is_poor_0) === 'co' ? '0' : null

                item.is_revolution = this.toSlug._ini(item.is_revolution) === 'co' ? '1' : null

                item.sex = this.toSlug._ini(item.sex) === 'nam' ? '1' : '0'
                item.status = '1'

                item.code = 'SSE_' + (++code).toLocaleString(undefined, { useGrouping: false, minimumIntegerDigits: 4 })
                code = +item.code.slice(4)

                delete item.type_idno_1
                delete item.type_idno_2
                delete item.type_idno_3
                delete item.is_poor_0
                delete item.is_poor_1
                delete item.isChecked
                delete item.isValid

                return item
            })

            this.globals.send({
                path: this.token.importStudents,
                token: 'importStudents',
                data: array
            })

            return
        })
    }

    calculateChecked = (): number => this.cstable.data.filter(item => item.isChecked).length
}
