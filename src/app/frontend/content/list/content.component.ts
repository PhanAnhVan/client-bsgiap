import { Component, Input, OnChanges, OnDestroy, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { PageChangedEvent } from 'ngx-bootstrap/pagination'
import { Subscription } from 'rxjs'
import { Globals } from 'src/app/globals'

@Component({
    selector: 'app-content-list',
    templateUrl: './content.component.html',
    styleUrls: ['./content.component.scss']
})
export class ListContentComponent implements OnInit, OnChanges, OnDestroy {
    @Input('link') link!: string

    private connect
    initialState: number = -1

    data: any = {}

    private token: any = {
        getContent: 'api/content/list',
        getContentGroup: 'api/content/group',
        getNewsMostView: 'api/content/asidebarRight'
    }

    constructor(public globals: Globals, public route: ActivatedRoute, public router: Router) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'getContent':
                    this.data = res.data
                    this.initialState = this.data.list?.length ? 1 : 0
                    this.pagination.ini(this.data.list)
                    break

                case 'getNewsMostView':
                    this.mostview.data = res.data
                    break

                default:
                    break
            }
        })
    }

    ngOnInit() {}

    ngOnChanges() {
        if (this.link && this.link != '') {
            this.globals.send({
                path: this.token.getContent,
                token: 'getContent',
                params: { link: this.link }
            })
        }
        this.mostview.send()
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    mostview = {
        data: [],
        send: () =>
            this.globals.send({
                path: this.token.getNewsMostView,
                token: 'getNewsMostView',
                params: {
                    limit: 5,
                    type: 'view'
                }
            })
    }

    pagination = {
        data: [],
        total: 0,
        maxSize: 5,
        itemsPerPage: 6,
        ini: (data: []) => {
            this.pagination.data = data
            this.pagination.total = data.length
            this.data.list = data.slice(0, this.pagination.itemsPerPage)
        },
        change: (e: PageChangedEvent) => {
            const start = (e.page - 1) * e.itemsPerPage
            const end = e.page * e.itemsPerPage

            this.data.list = this.pagination.data.slice(start, end)
            window.scrollTo({ top: 0, behavior: 'smooth' })
        }
    }
}
