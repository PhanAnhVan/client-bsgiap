import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { RouterModule, Routes } from '@angular/router'
import { TranslateModule } from '@ngx-translate/core'
import { LazyLoadImageModule } from 'ng-lazyload-image'
import { Ng5SliderModule } from 'ng5-slider'
import { CarouselModule } from 'ngx-bootstrap/carousel'
import { CollapseModule } from 'ngx-bootstrap/collapse'
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker'
import { BsDropdownModule } from 'ngx-bootstrap/dropdown'
import { ModalModule } from 'ngx-bootstrap/modal'
import { PaginationModule } from 'ngx-bootstrap/pagination'
import { TabsModule } from 'ngx-bootstrap/tabs'
import { TimepickerModule } from 'ngx-bootstrap/timepicker'
import { TypeaheadModule } from 'ngx-bootstrap/typeahead'
import { CarouselModule as OwlCarouselModule } from 'ngx-owl-carousel-o'
import { UserAuthGuard } from '../services/auth/userAuth.guard'
import { BindSrcDirective } from '../services/directive/bindSrc.directive'
import { DetailContentComponent } from './content/detail-content/detail-content.component'
import { ListContentComponent } from './content/list/content.component'
import { DocumentComponent } from './page/document/document.component'
import { FrontendComponent } from './frontend.component'
import { HomeComponent } from './home/home.component'
import { ModalSinginSingupComponent } from './components/modal-singin-singup/modal-singin-singup.component'
import { BoxContentGridComponent } from './components/box-content-grid/box-content-grid.component'
import { BoxContentComponent } from './components/box-content/box-content.component'
import { BoxProductComponent } from './components/box-product/box-product.component'
import { BreadcrumbComponent } from './components/breadcrumb/breadcrumb.component'
import { CommentComponent } from './components/comment/comment.component'
import { CompanyComponent } from './page/company/company.component'
import { FormComponent } from './modules/fm/fm.component'
import { FooterComponent } from './modules/footer/footer.component'
import { HeaderComponent } from './modules/header/header.component'
import { LoadingComponent } from './components/loading/loading.component'
import { MenuHorizontalComponent } from './modules/menu-horizontal/menu-horizontal.component'
import { MenuComponent } from './modules/menu/menu.component'
import { NotFoundComponent } from './page/not-found/not-found.component'
import { PageComponent } from './page/page.component'
import { DetailProductComponent } from './product/detail-product/detail-product.component'
import { ListProductComponent } from './product/list/product.component'
import { SanitizeHtmlPipe } from './sanitizeHtml.pipe'
import { SearchComponent } from './page/search/search.component'
import { SigninComponent } from './user/signin/signin.component'
import { SignupComponent } from './user/signup/signup.component'
import { HomeSliderComponent } from './home/slider/slider.component'
import { HomeAboutUsComponent } from './home/aboutUs/aboutUs.component'
import { HomeServicesComponent } from './home/services/services.component'
import { BoxServicesComponent } from './components/box-services/box-services.component'
import { HomeNewsComponent } from './home/news/news.component'
import { BoxNewsComponent } from './components/box-news/box-news.component'
import { HomeSayAboutUsComponent } from './home/sayAboutUs/sayAboutUs.component'
import { BoxSayAboutUsComponent } from './components/box-sayAboutUs/box-sayAboutUs.component'
import { BoxBreadcrumbComponent } from './components/box-breadcrumb/box-breadcrumb.component'
import { ContactComponent } from './page/contact/contact.component'
import { ModalAboutUsComponent } from './components/modal-aboutUs/modal-aboutUs.component'

const appRoutes: Routes = [
    {
        path: '',
        component: FrontendComponent,
        children: [
            { path: 'trang-chu', redirectTo: '' },
            { path: '', component: HomeComponent },
            { path: '404', component: NotFoundComponent },
            { path: 'tim-kiem', component: SearchComponent },
            { path: 'dang-nhap', component: SigninComponent },
            { path: 'tai-lieu', component: DocumentComponent },
            { path: 'doanh-nghiep', component: CompanyComponent },
            { path: 'lien-he', component: ContactComponent },
            {
                path: 'user',
                loadChildren: () => import('./user/user.module').then(m => m.UserModule)
            },
            { path: ':link', component: PageComponent },
            { path: ':parent_link/:link', component: PageComponent },
            { path: ':parent_links/:parent_link/:link', component: DetailContentComponent }
        ]
    },
    { path: '**', redirectTo: '404' }
]

@NgModule({
    imports: [
        LazyLoadImageModule,
        CommonModule,
        FormsModule,
        TranslateModule,
        ReactiveFormsModule,
        CollapseModule,
        TabsModule.forRoot(),
        RouterModule.forChild(appRoutes),
        TimepickerModule.forRoot(),
        BsDatepickerModule.forRoot(),
        ModalModule.forRoot(),
        CarouselModule,
        Ng5SliderModule,
        OwlCarouselModule,
        TypeaheadModule.forRoot(),
        PaginationModule.forRoot(),
        BsDropdownModule.forRoot()
    ],
    declarations: [
        FrontendComponent,
        HomeComponent,
        HeaderComponent,
        FooterComponent,
        PageComponent,
        MenuComponent,
        MenuHorizontalComponent,
        CommentComponent,
        DetailContentComponent,
        BoxContentComponent,
        BoxContentGridComponent,
        ListContentComponent,
        ListProductComponent,
        SanitizeHtmlPipe,
        BindSrcDirective,
        SearchComponent,
        DetailProductComponent,
        NotFoundComponent,
        BoxProductComponent,
        SigninComponent,
        SignupComponent,
        FormComponent,
        ModalSinginSingupComponent,
        DocumentComponent,
        BreadcrumbComponent,
        LoadingComponent,
        CompanyComponent,

        HomeSliderComponent,
        HomeAboutUsComponent,
        HomeServicesComponent,
        HomeNewsComponent,
        HomeSayAboutUsComponent,

        BoxServicesComponent,
        BoxNewsComponent,
        BoxSayAboutUsComponent,
        BoxBreadcrumbComponent,
        ModalAboutUsComponent,

        ContactComponent,
    ],
    providers: [UserAuthGuard],
    entryComponents: [ModalSinginSingupComponent]
})
export class FrontendModule {}
