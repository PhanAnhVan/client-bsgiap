import { Component, Input } from '@angular/core';
import { DEFAULT_IMAGE } from '../../core';

@Component({
    selector: 'app-box-content',
    templateUrl: './box-content.component.html',
    styleUrls: ['./box-content.component.scss']
})
export class BoxContentComponent {
    @Input('input') item
    @Input('type') type
    defaultImage = DEFAULT_IMAGE

    constructor() {}

    ngOnInit() {}
}