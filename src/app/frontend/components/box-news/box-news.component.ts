import { Component } from '@angular/core'

@Component({
    selector: 'app-box-news',
    templateUrl: './box-news.component.html',
    styleUrls: ['./box-news.component.scss']
})
export class BoxNewsComponent {}
