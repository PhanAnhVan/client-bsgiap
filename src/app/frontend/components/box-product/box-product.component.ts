import { Component, Input, OnInit } from '@angular/core';
import { DEFAULT_IMAGE } from '../../core';

@Component({
    selector: 'app-box-product',
    templateUrl: './box-product.component.html',
    styleUrls: ['./box-product.component.scss']
})
export class BoxProductComponent implements OnInit {
    @Input('input') item
    defaultImage = DEFAULT_IMAGE

    constructor() {}

    calculateDiff = (maker_date: string) => {
        let date = new Date(maker_date)
        let today = new Date()

        let days_ago = (today.getTime() - date.getTime()) / (1000 * 3600 * 24)

        return parseInt(days_ago.toString())
    }

    ngOnInit() {}
}
