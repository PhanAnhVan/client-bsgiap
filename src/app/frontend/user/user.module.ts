import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { RouterModule, Routes } from '@angular/router'
import { TranslateModule } from '@ngx-translate/core'
import { UserAuthGuard } from './../../services/auth/userAuth.guard'
import { SanitizeHtmlPipe } from './sanitizeHtml.pipe'

import { UserComponent } from './user.component'
import { InfoComponent } from './info/info.component'
import { ListClassComponent } from './list-class/list-class.component'
import { ChangePasswordComponent } from './change-password/change-password.component'

const appRoutes: Routes = [
    {
        path: '',
        component: UserComponent,
        canActivate: [UserAuthGuard],
        children: [
            { path: 'hoc-vien', component: InfoComponent },
            { path: 'lop-hoc', component: ListClassComponent },
            { path: 'doi-mat-khau', component: ChangePasswordComponent }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(appRoutes), CommonModule, FormsModule, ReactiveFormsModule, TranslateModule],
    declarations: [UserComponent, InfoComponent, ListClassComponent, ChangePasswordComponent, SanitizeHtmlPipe]
})
export class UserModule {}
