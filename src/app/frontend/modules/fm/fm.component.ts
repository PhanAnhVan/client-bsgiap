import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router'
import { TranslateService } from '@ngx-translate/core'
import { AuthService } from 'angularx-social-login'
import { ToastrService } from 'ngx-toastr'
import { Globals } from 'src/app/globals'

@Component({
    selector: 'app-fm',
    templateUrl: './fm.component.html',
    styleUrls: ['./fm.component.scss'],
    providers: [AuthService]
})
export class FormComponent implements OnInit, OnDestroy {
    private connect

    @Output('event') eventOutput = new EventEmitter()
    @Input('type') typeInput: number = 0
    @Input('component') component: string = ''

    fm: FormGroup
    fmResetPassword: FormGroup
    fmRegisterCourse: FormGroup
    type: number = 1 // TODO: 1 - LOGIN, 2 - RESET PASSWORD, 3 - COMPANY SIGNIN, 4 - REGISTER COURSE
    flag: boolean = true

    constructor(
        public fb: FormBuilder,
        private toastr: ToastrService,
        public router: Router,
        public routerAct: ActivatedRoute,
        public globals: Globals,
        public translate: TranslateService
    ) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'resetPassword':
                    this.resetPassword.state = false
                case 'loginCustommer':
                    this.showNotification(res)

                    if (res.status !== 1) return
                    this.globals.CUSTOMER.set(res.data)

                    this.eventOutput.emit(true)
                    this.router.navigate(['/user/hoc-vien'])
                    break

                case 'registerCourse':
                    this.showNotification(res)
                    this.eventOutput.emit(true)
                    break

                case 'companySignin':
                    this.showNotification(res)
                    if (res.status !== 1) return this.eventOutput.emit(false)

                    this.eventOutput.emit(true)
                    this.globals.crypto.link('doanh-nghiep', { code: res.data })
                    break

                case 'getAllMajors':
                    this.registerCourse.listMajors = res.data
                    this.registerCourse.fmConfigs(this.registerCourse.listMajors)
                    break

                default:
                    break
            }
        })
    }

    ngOnInit() {
        this.login.fmConfigs()

        this.resetPassword.fmConfigs()

        this.registerCourse.fmConfigs()

        this.type = +this.typeInput > 0 ? +this.typeInput : this.type
        ;+this.type === 4 && this.registerCourse.getAllMajors()
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    onChange = type => (this.type = +type)

    showNotification(res) {
        let type = res.status == 1 ? 'success' : res.status == 0 ? 'warning' : 'danger'

        return this.toastr[type](res.message, type, { timeOut: 1500 })
    }

    login = {
        token: 'api/login/customer',
        type: 'password',
        fmConfigs: (item: any = '') => {
            item = typeof item === 'object' ? item : {}
            this.fm = this.fb.group({
                code: item.code ? item.code : '',
                password: [item.password ? item.password : '', [Validators.required]]
            })
        },
        onSubmit: () => {
            if (this.fm.valid) {
                this.globals.send({
                    path: this.login.token,
                    token: 'loginCustommer',
                    data: this.fm.value
                })
            }
        }
    }

    registerCourse = {
        token: 'api/contact/add',
        listMajors: [],
        getAllMajors: () => {
            this.globals.send({
                path: 'api/contact/getAllMajors',
                token: 'getAllMajors'
            })
        },
        fmConfigs: (item: any = '') => {
            item = typeof item === 'object' ? item : {}

            this.fmRegisterCourse = this.fb.group({
                name: ['', [Validators.required]],
                email: [
                    '',
                    [
                        Validators.pattern(/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i)
                    ]
                ],
                phone: [
                    '',
                    [
                        Validators.required,
                        Validators.pattern(/^([_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,5}))|(\d+$)$/)
                    ]
                ],
                message: [''],
                page_id: [item.id ? +item.id : 0, [Validators.required]]
            })
        },
        onSubmit: () => {
            if (this.fmRegisterCourse.valid) {
                this.globals.send({
                    path: this.registerCourse.token,
                    token: 'registerCourse',
                    data: this.fmRegisterCourse.value
                })
            }
        }
    }

    resetPassword = {
        state: false,
        token: 'api/customer/resetPassword',
        fmConfigs: (item: any = '') => {
            item = typeof item === 'object' ? item : {}
            this.fmResetPassword = this.fb.group({
                email: [
                    '',
                    [Validators.required, Validators.pattern(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)]
                ]
            })
        },
        onSubmit: () => {
            let data = this.fmResetPassword.value
            if (this.flag == true && this.fmResetPassword.valid) {
                this.flag = false
                this.resetPassword.state = true
                this.globals.send({
                    path: this.resetPassword.token,
                    token: 'resetPassword',
                    data: data
                })
            }
        }
    }

    company = {
        value: '',
        onSubmit: () => {
            if (!this.company.value.length) return

            this.globals.send({
                path: 'api/customer/companySignin',
                token: 'companySignin',
                params: { code: this.company.value }
            })

            this.company.value = ''
        }
    }
}
